from flask import Flask

app = Flask(__name__)


@app.route("/")
def index():
    return "All Posts!"


@app.route("/post/<slug>")
def show(slug):
    return "Show Post: '%s'" % slug


@app.route("/post/<slug>/edit")
def edit(slug):
    return "Edit Post: '%s'" % slug


@app.route('/post/new')
def new():
    return "New Post"


@app.route("/post/<slug>/delete")
def delete(slug):
    return "Delete Post: '%s'" % slug


if __name__ == "__main__":
    app.run(debug=True)
